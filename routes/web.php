<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->get('/', ['uses' => 'GroupsController@homepage', 'as' => 'homepage']);

$app->get('/groups/hierarchy', ['uses' => 'GroupsController@hierarchy', 'as' => 'groups.hierarchy']);
$app->get('/groups', ['uses' => 'GroupsController@index', 'as' => 'groups.index']);
$app->get('/groups/{id}', ['uses' => 'GroupsController@show', 'as' => 'groups.show']);

$app->get('/postcodes/{id}', ['uses' => 'PostcodesController@show', 'as' => 'postcodes.show']);

$app->get('/postcodes/{id}/busstops/nearby', ['uses' => 'PostcodesBusstopsController@nearby', 'as' => 'postcodes.busstops.nearby']);
$app->get('/postcodes/{id}/schools/within', ['uses' => 'PostcodesSchoolsController@within', 'as' => 'postcodes.schools.within']);
$app->get('/postcodes/{id}/addresses', ['uses' => 'PostcodesAddressesController@index', 'as' => 'postcodes.addresses.index']);

$app->get('/busstops/{id}', ['uses' => 'BusstopsController@show', 'as' => 'busstops.show']);