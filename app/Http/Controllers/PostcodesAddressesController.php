<?php

namespace App\Http\Controllers;

use App\Repositories\AddressesRepository;
use App\Repositories\PostcodesRepository;
use App\Transformers\AddressTransformer;
use Illuminate\Http\Response;
use League\Fractal\Resource\Collection;

class PostcodesAddressesController extends Controller
{
    /**
     * @var PostcodesRepository
     */
    private $postcodesRepository;
    /**
     * @var AddressesRepository
     */
    private $addressesRepository;

    /**
     * PostcodesAddressesController constructor.
     * @param PostcodesRepository $postcodesRepository
     * @param AddressesRepository $addressesRepository
     */
    public function __construct(PostcodesRepository $postcodesRepository, AddressesRepository $addressesRepository)
    {
        $this->postcodesRepository = $postcodesRepository;
        $this->addressesRepository = $addressesRepository;
        parent::__construct();
    }

    public function index($id)
    {
        $postcode = $this->postcodesRepository->find($id);

        if ($postcode == null) {
            return response()->json([
                'error' => "Postcode not found",
            ], Response::HTTP_NOT_FOUND);
        }

        $addresses = $this->addressesRepository->allOnPostcode($postcode->getId());

        return response()->json([
            'data' => $this->manager->createData(new Collection($addresses, new AddressTransformer))->toArray(),
        ]);
    }
}