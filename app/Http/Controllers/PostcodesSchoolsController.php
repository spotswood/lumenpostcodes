<?php

namespace App\Http\Controllers;

use App\Repositories\PostcodesRepository;
use App\Repositories\SchoolsRepository;
use App\Transformers\SchoolTransformer;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use League\Fractal\Resource\Collection;

class PostcodesSchoolsController extends Controller
{
    /**
     * @var PostcodesRepository
     */
    private $postcodesRepository;
    /**
     * @var SchoolsRepository
     */
    private $schoolsRepository;

    /**
     * PostcodesSchoolsController constructor.
     * @param PostcodesRepository $postcodesRepository
     * @param SchoolsRepository $schoolsRepository
     */
    public function __construct(PostcodesRepository $postcodesRepository, SchoolsRepository $schoolsRepository)
    {
        $this->postcodesRepository = $postcodesRepository;
        $this->schoolsRepository = $schoolsRepository;
        parent::__construct();
    }

    public function within(Request $request, $id)
    {
        $postcode = $this->postcodesRepository->find($id);

        if ($postcode == null) {
            return response()->json([
                'error' => "Postcode not found",
            ], Response::HTTP_NOT_FOUND);
        }

        $schoolsWithin = $this->schoolsRepository->schoolsWithinRadius($postcode->getLatitude(), $postcode->getLongitude());

        return response()->json([
            'data' => $this->manager->createData(new Collection($schoolsWithin, new SchoolTransformer))->toArray(),
        ]);
    }
}