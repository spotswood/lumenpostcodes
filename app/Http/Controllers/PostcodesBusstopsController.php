<?php

namespace App\Http\Controllers;

use App\Repositories\BusstopsRepository;
use App\Repositories\PostcodesRepository;
use App\Transformers\BusstopTransformer;
use App\Transformers\PostcodeTransformer;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;

class PostcodesBusstopsController extends Controller
{
    /**
     * @var PostcodesRepository
     */
    private $postcodesRepository;

    /**
     * @var BusstopsRepository
     */
    private $busstopsRepository;

    /**
     * PostcodesBusstopsController constructor.
     * @param PostcodesRepository $postcodesRepository
     * @param BusstopsRepository $busstopsRepository
     */
    public function __construct(PostcodesRepository $postcodesRepository, BusstopsRepository $busstopsRepository)
    {
        $this->postcodesRepository = $postcodesRepository;
        $this->busstopsRepository = $busstopsRepository;
        parent::__construct();
    }

    public function nearby(Request $request, $postcodeId)
    {
        $postcode = $this->postcodesRepository->find($postcodeId);

        if ($postcode == null) {
            return response()->json([
                'error' => "Postcode not found",
            ], Response::HTTP_NOT_FOUND);
        }

        $amount = (int) $request->get('amount');
        if ($amount < 1) {
            $amount = 5;
        }


        $nearbyBusstops = $this->busstopsRepository->nearbyBusstops($postcode->getLatitude(), $postcode->getLongitude(), $amount);

        return response()->json([
            'data' => $this->manager->createData(new Collection($nearbyBusstops, new BusstopTransformer))->toArray(),
        ]);
    }
}
