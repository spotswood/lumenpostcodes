<?php

namespace App\Http\Controllers;

use App\Transformers\SimpleArraySerializer;
use Laravel\Lumen\Routing\Controller as BaseController;
use League\Fractal\Manager;

class Controller extends BaseController
{
    /**
     * @var Manager
     */
    protected $manager;

    /**
     * Controller constructor.
     */
    public function __construct()
    {
        $this->manager = new Manager();
        $this->manager->setSerializer(new SimpleArraySerializer());
    }
}
