<?php

namespace App\Http\Controllers;

use App\Repositories\SchoolsRepository;
use Illuminate\Http\Response;

class SchoolsController extends Controller
{
    /**
     * @var SchoolsRepository
     */
    private $schoolsRepository;

    /**
     * SchoolsController constructor.
     * @param SchoolsRepository $schoolsRepository
     */
    public function __construct(SchoolsRepository $schoolsRepository)
    {
        $this->schoolsRepository = $schoolsRepository;
        parent::__construct();
    }

    public function show($id)
    {
        $school = $this->schoolsRepository->find($id);

        if ($school == null) {
            return response()->json([
                'error' => "Postcode not found",
            ], Response::HTTP_NOT_FOUND);
        }
    }
}