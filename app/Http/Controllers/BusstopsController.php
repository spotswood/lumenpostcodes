<?php

namespace App\Http\Controllers;

use App\Repositories\BusstopsRepository;
use App\Transformers\BusstopTransformer;
use Illuminate\Http\Response;
use League\Fractal\Resource\Item;

class BusstopsController extends Controller
{
    /**
     * @var BusstopsRepository
     */
    private $busstopsRepository;

    /**
     * BusstopController constructor.
     * @param BusstopsRepository $busstopsRepository
     */
    public function __construct(BusstopsRepository $busstopsRepository)
    {
        $this->busstopsRepository = $busstopsRepository;
        parent::__construct();
    }

    public function show($id)
    {
        $busstop = $this->busstopsRepository->find($id);

        if ($busstop == null) {
            return response()->json([
                'error' => "Postcode not found",
            ], Response::HTTP_NOT_FOUND);
        }

        return response()->json([
            'data' => $this->manager->createData(new Item($busstop, new BusstopTransformer))->toArray(),
        ]);
    }
}