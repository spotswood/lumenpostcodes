<?php

namespace App\Http\Controllers;

use App\Repositories\PostcodesRepository;
use App\Transformers\PostcodeTransformer;
use Illuminate\Http\Response;
use League\Fractal\Resource\Item;

class PostcodesController extends Controller
{
    /**
     * @var PostcodesRepository
     */
    private $postcodesRepository;

    /**
     * PostcodesController constructor.
     * @param PostcodesRepository $postcodesRepository
     */
    public function __construct(PostcodesRepository $postcodesRepository)
    {
        $this->postcodesRepository = $postcodesRepository;
        parent::__construct();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function show($id)
    {
        $postcode = $this->postcodesRepository->find($id);

        if ($postcode == null) {
            return response()->json([
                'error' => "Postcode not found",
            ], Response::HTTP_NOT_FOUND);
        }

        return response()->json([
            'data' => $this->manager->createData(new Item($postcode, new PostcodeTransformer))->toArray(),
        ]);
    }
}