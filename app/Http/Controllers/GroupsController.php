<?php

namespace App\Http\Controllers;

use App\Models\Group;
use App\Repositories\GroupsRepository;
use App\Transformers\GroupTransformer;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;

/**
 * Class GroupsController
 * @package App\Http\Controllers
 */
class GroupsController extends Controller
{
    /**
     * @var GroupsRepository
     */
    private $groupsRepository;


    /**
     * GroupsController constructor.
     * @param GroupsRepository $groupsRepository
     */
    public function __construct(GroupsRepository $groupsRepository)
    {
        $this->groupsRepository = $groupsRepository;
        parent::__construct();
    }

    public function homepage()
    {
        return view('homepage');
    }
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $groups = $this->groupsRepository->all();

        return response()->json([
            'data' => $this->manager->createData(new Collection($groups, new GroupTransformer))->toArray(),
            'total' => count($groups),
        ]);
    }

    public function show($id)
    {
        $group = $this->groupsRepository->find($id);

        if ($group == null) {
            return response()->json([
                'error' => "Group not found",
            ], Response::HTTP_NOT_FOUND);
        }

        return response()->json([
            'data' => $this->manager->createData(new Item($group, new GroupTransformer))->toArray(),
        ]);
    }

    public function hierarchy()
    {
        $hierarchy = $this->groupsRepository->allGroupsHierarchy();

        return view('hierarchy')->with('hierarchy', $hierarchy);
    }
}
