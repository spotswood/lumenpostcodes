<?php

namespace App\Models;

/**
 * Class Busstop
 * @package App\Models
 */
class Busstop
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return Postcode
     */
    public function getPostcode(): Postcode
    {
        return $this->postcode;
    }

    /**
     * @param Postcode $postcode
     */
    public function setPostcode(Postcode $postcode)
    {
        $this->postcode = $postcode;
    }

    /**
     * @var Postcode
     */
    private $postcode;
}