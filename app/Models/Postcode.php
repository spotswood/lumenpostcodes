<?php

namespace App\Models;

/**
 * Class Postcode
 * @package App\Models
 */
class Postcode
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $postcode;

    /**
     * @var double
     */
    private $latitude;
    /**
     * @var double
     */

    private $longitude;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getPostcode(): string
    {
        return $this->postcode;
    }

    /**
     * @param string $postcode
     */
    public function setPostcode(string $postcode)
    {
        $this->postcode = $postcode;
    }

    /**
     * @return float
     */
    public function getLatitude(): float
    {
        return $this->latitude;
    }

    /**
     * @param float $latitude
     */
    public function setLatitude(float $latitude)
    {
        $this->latitude = $latitude;
    }

    /**
     * @return float
     */
    public function getLongitude(): float
    {
        return $this->longitude;
    }

    /**
     * @param float $longitude
     */
    public function setLongitude(float $longitude)
    {
        $this->longitude = $longitude;
    }

    /**
     * @return Group
     */
    public function getGroup(): Group
    {
        return $this->group;
    }

    /**
     * @param Group $group
     */
    public function setGroup(Group $group)
    {
        $this->group = $group;
    }

    /**
     * @var Group
     */
    private $group;

    /**
     * @return Busstop[]
     */
    public function getBusstops(): array
    {
        return $this->busstops;
    }

    /**
     * @param Busstop[] $busstops
     */
    public function setBusstops(array $busstops)
    {
        $this->busstops = $busstops;
    }

    /**
     * @var Busstop[]
     */
    private $busstops;

    public function addBusstop(Busstop $busstop)
    {
        $this->busstops []= $busstop;
    }
}