<?php

namespace App\Models;

/**
 * Class School
 * @package App\Models
 */
class School
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var int
     */
    private $phase;

    /**
     * @var int
     */
    private $rating;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getPhase(): int
    {
        return $this->phase;
    }

    /**
     * @param int $phase
     */
    public function setPhase(int $phase)
    {
        $this->phase = $phase;
    }

    /**
     * @return int
     */
    public function getRating(): int
    {
        return $this->rating;
    }

    /**
     * @param int $rating
     */
    public function setRating(int $rating)
    {
        $this->rating = $rating;
    }

    /**
     * @return Postcode
     */
    public function getPostcode(): Postcode
    {
        return $this->postcode;
    }

    /**
     * @param Postcode $postcode
     */
    public function setPostcode(Postcode $postcode)
    {
        $this->postcode = $postcode;
    }

    /**
     * @var Postcode
     */
    private $postcode;

}