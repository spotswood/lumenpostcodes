<?php

namespace App\Models;

/**
 * Class Group
 * @package App\Models
 */
class Group
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var Subgroup[]
     */
    private $subgroups;

    /**
     * Group constructor.
     * @param int $id
     * @param string $name
     */
    public function __construct($id, $name)
    {
        $this->id = $id;
        $this->name = $name;
        $this->subgroups = [];
    }

    /**
     * @return array
     */
    public function getSubgroups(): array
    {
        return $this->subgroups;
    }

    /**
     * @param array $subgroups
     */
    public function setSubgroups(array $subgroups)
    {
        $this->subgroups = $subgroups;
    }

    /**
     * @param Subgroup $subgroup
     * @throws \Exception
     */
    public function addSubgroup(Subgroup $subgroup)
    {
        if ($this->hasSubgroup($subgroup)) {
            throw new \Exception("Duplicate subgroup");
        }
        $this->subgroups []= $subgroup;
    }


    /**
     * @param $subgroup
     * @return bool
     */
    public function hasSubgroup($subgroup) : bool
    {
        if ($subgroup instanceof Subgroup) {
            return $this->hasSubgroupByObjectName($subgroup);
        }
        return $this->hasSubgroupByStringName($subgroup);
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @param Subgroup $subgroup
     * @return bool
     */
    private function hasSubgroupByObjectName(Subgroup $subgroup): bool
    {
        foreach ($this->subgroups as $current) {
            if ($current->getName() == $subgroup->getName()) {
                return true;
            }
        }
        return false;
    }

    /**
     * @param $subgroupName
     * @return bool
     */
    private function hasSubgroupByStringName($subgroupName): bool
    {
        foreach ($this->subgroups as $current) {
            if ($current->getName() == $subgroupName) {
                return true;
            }
        }
        return false;
    }

    public function getSubgroupByName(string $subgroup)
    {
        foreach ($this->subgroups as $current) {
            if ($current->getName() == $subgroup) {
                return $current;
            }
        }
        return null;
    }

    public function getTotalPostcodesCount()
    {
        $total = 0;
        foreach ($this->subgroups as $subgroup) {
            $total += count($subgroup->getPostcodes());
        }
        return $total;
    }
}