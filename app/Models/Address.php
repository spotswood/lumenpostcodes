<?php

namespace App\Models;

/**
 * Class Address
 * @package App\Models
 */
class Address
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $district;

    /**
     * @var string
     */
    private $locality;

    /**
     * @var string
     */
    private $street;

    /**
     * @var string
     */
    private $site;

    /**
     * @var string
     */
    private $siteNumber;

    /**
     * @var string
     */
    private $siteDescription;

    /**
     * @var string
     */
    private $siteSubDescription;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getDistrict(): string
    {
        return $this->district;
    }

    /**
     * @param string $district
     */
    public function setDistrict(string $district)
    {
        $this->district = $district;
    }

    /**
     * @return string
     */
    public function getLocality(): string
    {
        return $this->locality;
    }

    /**
     * @param string $locality
     */
    public function setLocality(string $locality)
    {
        $this->locality = $locality;
    }

    /**
     * @return string
     */
    public function getStreet(): string
    {
        return $this->street;
    }

    /**
     * @param string $street
     */
    public function setStreet(string $street)
    {
        $this->street = $street;
    }

    /**
     * @return string
     */
    public function getSite(): string
    {
        return $this->site;
    }

    /**
     * @param string $site
     */
    public function setSite(string $site)
    {
        $this->site = $site;
    }

    /**
     * @return string
     */
    public function getSiteNumber(): string
    {
        return $this->siteNumber;
    }

    /**
     * @param string $siteNumber
     */
    public function setSiteNumber(string $siteNumber)
    {
        $this->siteNumber = $siteNumber;
    }

    /**
     * @return string
     */
    public function getSiteDescription(): string
    {
        return $this->siteDescription;
    }

    /**
     * @param string $siteDescription
     */
    public function setSiteDescription(string $siteDescription)
    {
        $this->siteDescription = $siteDescription;
    }

    /**
     * @return string
     */
    public function getSiteSubDescription(): string
    {
        return $this->siteSubDescription;
    }

    /**
     * @param string $siteSubDescription
     */
    public function setSiteSubDescription(string $siteSubDescription)
    {
        $this->siteSubDescription = $siteSubDescription;
    }

    /**
     * @return Postcode
     */
    public function getPostcode(): Postcode
    {
        return $this->postcode;
    }

    /**
     * @param Postcode $postcode
     */
    public function setPostcode(Postcode $postcode)
    {
        $this->postcode = $postcode;
    }

    /**
     * @var Postcode
     */
    private $postcode;
}