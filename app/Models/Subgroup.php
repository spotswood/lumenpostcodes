<?php

namespace App\Models;


/**
 * Class Subgroup
 * @package App\Models
 */
class Subgroup
{
    /**
     * @var string
     */
    protected $name;

    /**
     * Subgroup constructor.
     * @param string $name
     */
    public function __construct($name)
    {
        $this->name = $name;
        $this->postcodes = [];
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return Postcode[]
     */
    public function getPostcodes(): array
    {
        return $this->postcodes;
    }

    /**
     * @param Postcode[] $postcodes
     */
    public function setPostcodes(array $postcodes)
    {
        $this->postcodes = $postcodes;
    }

    /**
     * @param Postcode $postcode
     */
    public function addPostcode(Postcode $postcode)
    {
        $this->postcodes []= $postcode;
    }

    public function hasPostcode(Postcode $postcode)
    {
        foreach ($this->postcodes as $current) {
            if ($current->getId() == $postcode->getId()) {
                return true;
            }
        }
        return false;
    }

    /**
     * @var Postcode[]
     */
    protected $postcodes;

}