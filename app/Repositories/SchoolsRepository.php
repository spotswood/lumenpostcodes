<?php

namespace App\Repositories;

use App\Models\School;

class SchoolsRepository implements DomainRepository
{

    public function all()
    {
        return [];
    }

    public function find(int $id)
    {
        $schoolDBRow = app('db')->select("SELECT * FROM schools WHERE id = {$id} LIMIT 1");

        if (count($schoolDBRow) != 1) {
            return null;
        }

        $school = new School();
        $school->setId($schoolDBRow[0]->id);
        $school->setName($schoolDBRow[0]->name);
        $school->setPhase($schoolDBRow[0]->phase);
        $school->setRating($schoolDBRow[0]->rating);

        return $school;
    }

    public function schoolsWithinRadius(float $latitude, float $longitude, int $radius = 20) : array
    {

        $schoolsDBRows = app('db')->select("SELECT s.id, s.name, s.phase, s.rating, ( 6371 * acos( cos( radians({$latitude}) ) * cos( radians( p.latitude ) ) 
                                        * cos( radians( p.longitude ) - radians({$longitude}) ) + sin( radians({$latitude}) ) * 
                                        sin( radians( p.latitude ) ) ) ) AS distance FROM postcodes as p join schools as 
                                        s on p.id = s.postcode_id HAVING distance <= 20 ORDER BY distance");
        $schoolsWithin = [];

        foreach ($schoolsDBRows as $row) {
            $school = new School();
            $school->setId($row->id);
            $school->setName($row->name);
            $school->setPhase($row->phase);
            $school->setRating($row->rating);
            $schoolsWithin []= $school;
        }

        return $schoolsWithin;
    }

}