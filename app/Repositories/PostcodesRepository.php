<?php

namespace App\Repositories;

use App\Models\Postcode;

class PostcodesRepository implements DomainRepository
{

    public function all()
    {
        return [];
    }

    public function find(int $id)
    {
        $postcodeDBRow = app('db')->select("SELECT * FROM postcodes WHERE id = {$id} LIMIT 1");

        if (count($postcodeDBRow) != 1) {
            return null;
        }

        $postcode = new Postcode();
        $postcode->setId($postcodeDBRow[0]->id);
        $postcode->setPostcode($postcodeDBRow[0]->postcode);
        $postcode->setLatitude($postcodeDBRow[0]->latitude);
        $postcode->setLongitude($postcodeDBRow[0]->longitude);

        return $postcode;
    }
}