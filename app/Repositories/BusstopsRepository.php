<?php

namespace App\Repositories;

use App\Models\Busstop;

class BusstopsRepository implements DomainRepository
{

    public function all()
    {
        return [];
    }

    public function find(int $id)
    {
        $busstopDBRow = app('db')->select("SELECT * FROM busstops WHERE id = {$id} LIMIT 1");

        if (count($busstopDBRow) != 1) {
            return null;
        }

        $busstop = new Busstop();
        $busstop->setId($busstopDBRow[0]->id);
        $busstop->setName($busstopDBRow[0]->name);

        return $busstop;
    }

    public function nearbyBusstops(float $latitude, float $longitude, int $amount = 5) : array
    {

        $nearbyDBRow = app('db')->select("SELECT b.id, b.name, ( 6371 * acos( cos( radians({$latitude}) ) * cos( radians( p.latitude ) ) 
                                        * cos( radians( p.longitude ) - radians({$longitude}) ) + sin( radians({$latitude}) ) * 
                                        sin( radians( p.latitude ) ) ) ) AS distance FROM postcodes as p join busstops as 
                                        b on p.id = b.postcode_id ORDER BY distance LIMIT {$amount}");
        $nearbyCollection = [];

        foreach ($nearbyDBRow as $row) {
            $busstop = new Busstop();
            $busstop->setId($row->id);
            $busstop->setName($row->name);
            $nearbyCollection []= $busstop;
        }

        return $nearbyCollection;
    }

}