<?php

namespace App\Repositories;

use App\Models\Group;
use App\Models\Postcode;
use App\Models\Subgroup;

class GroupsRepository implements DomainRepository
{
    public function all() : array
    {
        $groupsDBRows = app('db')->select("SELECT * FROM groups");

        $groups = [];

        foreach ($groupsDBRows as $row) {
            $group = new Group($row->id, $row->name);

            $groups []= $group;
        }

        return $groups;
    }

    public function allGroupsHierarchy() : array
    {
        $groupsWithPostcodesDBRows = app('db')
            ->select("SELECT g.id, g.name,p.postcode, SUBSTRING_INDEX(p.postcode, ' ', 1) AS subgroup, p.id as postcode_id
                      FROM groups AS g JOIN postcodes AS p ON g.id = p.group_id
                      GROUP BY g.id, subgroup, p.postcode");

        $hierarchy = [];

        foreach ($groupsWithPostcodesDBRows as $row) {

            $postcode = new Postcode();
            $postcode->setId($row->postcode_id);
            $postcode->setPostcode($row->postcode);

            if ($this->collectionContainsGroupById($hierarchy, $row->id)) {
                $group = $this->getGroupByIdFromCollection($hierarchy, $row->id);

                if ($group->hasSubgroup($row->subgroup)) {
                    $subgroup = $group->getSubgroupByName($row->subgroup);
                    $subgroup->addPostcode($postcode);
                } else {
                    $subgroup = new Subgroup($row->subgroup);
                    $subgroup->addPostcode($postcode);
                    $group->addSubgroup($subgroup);
                }
            } else {
                $subgroup = new Subgroup($row->subgroup);
                $subgroup->addPostcode($postcode);

                $group = new Group($row->id, $row->name);
                $group->addSubgroup($subgroup);

                $hierarchy []= $group;
            }
        }

        return $hierarchy;
    }

    private function getGroupByIdFromCollection(array $groups, int $id)
    {
        foreach ($groups as $group) {
            if ($group->getId() == $id) {
                return $group;
            }
        }
        return null;
    }

    /**
     * @param array $groups
     * @param int $id
     * @return bool
     */
    private function collectionContainsGroupById(array $groups, int $id)
    {
        foreach ($groups as $group) {
            if ($group->getId() == $id) {
                return true;
            }
        }
        return false;
    }

    public function find(int $id)
    {
        $groupDBRow = app('db')->select("SELECT * FROM groups WHERE id = {$id} LIMIT 1");

        if (count($groupDBRow) != 1) {
            return null;
        }

        $group = new Group($groupDBRow[0]->id, $groupDBRow[0]->name);

        return $group;
    }
}