<?php

namespace App\Repositories;

interface DomainRepository
{
    public function all();
    public function find(int $id);
}