<?php

namespace App\Repositories;

use App\Models\Address;

class AddressesRepository implements DomainRepository
{

    public function all()
    {
        $addressDBRows = app('db')->select("SELECT * FROM addresses");
        
        $addresses = [];

        foreach ($addresses as $row) {
            $addresses = $this->newAddressFromRow($row);
        }

        return $addresses;
    }

    public function find(int $id)
    {
        $addressDBRows = app('db')->select("SELECT * FROM addresses WHERE id = {$id} LIMIT 1");

        if (count($addressDBRows) != 1) {
            return null;
        }

        return $this->newAddressFromRow($addressDBRows[0]);
    }

    public function allOnPostcode(int $postcodeId)
    {
        $addressDBRows = app('db')->select("SELECT a.id, a.district, a.locality, a.street, a.site, a.site, a.site_number, 
                                    a.site_description, a.site_subdescription FROM addresses as a join postcodes as p 
                                    on a.postcode_id = p.id WHERE p.id = {$postcodeId}");

        $addresses = [];

        foreach ($addressDBRows as $addressDBRow) {
            $addresses [] = $this->newAddressFromRow($addressDBRow);
        }

        return $addresses;
    }


    /**
     * @param \stdClass $addressDBRow
     * @return Address
     */
    private function newAddressFromRow(\stdClass $addressDBRow): Address
    {
        $address = new Address();
        
        $address->setId($addressDBRow->id);
        $address->setDistrict($addressDBRow->district);
        $address->setLocality($addressDBRow->locality);
        $address->setStreet($addressDBRow->street);
        $address->setSite($addressDBRow->site);
        $address->setSiteNumber($addressDBRow->site_number);
        $address->setSiteDescription($addressDBRow->site_description);
        $address->setSiteSubDescription($addressDBRow->site_subdescription);

        return $address;
    }
}