<?php

namespace App\Transformers;

use App\Models\School;
use League\Fractal\TransformerAbstract;

class SchoolTransformer extends TransformerAbstract
{
    public function transform(School $school)
    {
        return [
            'id' => $school->getId(),
            'name' => $school->getName(),
            'phase' => $school->getPhase(),
            'rating' => $school->getRating(),
        ];
    }
}