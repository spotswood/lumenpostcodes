<?php

namespace App\Transformers;

use App\Models\Busstop;
use League\Fractal\TransformerAbstract;

class BusstopTransformer extends TransformerAbstract
{
    public function transform(Busstop $busstop)
    {
        return [
            'id' => (int) $busstop->getId(),
            'name' => $busstop->getName(),
        ];
    }
}