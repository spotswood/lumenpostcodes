<?php

namespace App\Transformers;

use App\Models\Postcode;
use League\Fractal\TransformerAbstract;

class PostcodeTransformer extends TransformerAbstract
{
    public function transform(Postcode $postcode)
    {
        return [
            'id' => (int) $postcode->getId(),
            'coordinates' => [
                'latitude' => (int) $postcode->getLatitude(),
                'longitude' => (int) $postcode->getLatitude(),
            ],
        ];
    }
}