<?php

namespace App\Transformers;

use App\Models\Address;
use League\Fractal\TransformerAbstract;

class AddressTransformer extends TransformerAbstract
{
    public function transform(Address $address)
    {
        $street = $address->getStreet();
        $site = $address->getSite();
        $siteNumber = $address->getSiteNumber();
        $siteDescription = $address->getSiteDescription();
        $siteSubDescription = $address->getSiteSubDescription();

        $fullAddress = [];

        if ($street) {
            $fullAddress []= $street;
        }
        if ($siteNumber) {
            $fullAddress []= $siteNumber;
        }
        if ($siteDescription) {
            $fullAddress []= $siteDescription;
        }
        if ($siteSubDescription) {
            $fullAddress []= $siteSubDescription;
        }

        if (count($fullAddress) == 0) {
            $fullAddress = "";
        }

        $fullAddress = join(", ", $fullAddress);

        return [
            'id' => (int) $address->getId(),
            'district' => $address->getDistrict(),
            'locality' => $address->getLocality(),
            'street' => $street,
            'site' => [
                'name' => $site,
                'number' => $siteNumber,
                'description' => $siteDescription,
                'subdescription' => $siteSubDescription,
            ],
            'full' => $fullAddress,
        ];
    }
}