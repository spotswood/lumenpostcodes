$('.btn-postcode').on('click', function () {
    var postcodeId = $(this).attr('id');
    app.getInfo(postcodeId);
});

app = new Vue({
    el: '#sidebar',
    data: {
        message: "Click on postcode to get info",
        pristine: true,
        postcode: {},
        busstops: [],
        schools: [],
        addresses: []
    },
    methods: {
        getInfo: function (postcodeId) {
            this.pristine = false;
            this.getPostcode(postcodeId);
            this.getNearbyBusstops(postcodeId);
            this.getSchoolsWithin(postcodeId);
            this.getAddresses(postcodeId);
        },
        getPostcode: function (postcodeId) {
            axios.get('postcodes/' + postcodeId)
                .then(function (response) {
                    this.postcode = response.data.data;
                }.bind(this))
                .catch(function (error) {
                    console.log(error);
                });
        },
        getNearbyBusstops: function (postcodeId) {
            axios.get('postcodes/' + postcodeId + '/busstops/nearby')
                .then(function (response) {
                    this.busstops = response.data.data;
                }.bind(this))
                .catch(function (error) {
                    console.log(error);
                });
        },
        getSchoolsWithin: function (postcodeId) {
            axios.get('postcodes/'+postcodeId+'/schools/within')
                .then(function (response) {
                    this.schools = response.data.data;
                }.bind(this))
                .catch(function (error) {
                    console.log(error);
                });
        },
        getAddresses: function (postcodeId) {
            axios.get('postcodes/'+postcodeId+'/addresses')
                .then(function (response) {
                    this.addresses = response.data.data;
                }.bind(this))
                .catch(function (error) {
                    console.log(error);
                });
        }
    }
});