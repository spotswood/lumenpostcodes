<div id="sidebar">
    <h3 v-if="pristine">{{ message }}</h3>
    <h3 v-if="!pristine">Info for postcode id {{ postcode.id }}</h3>
    <hr>

    <div v-if="!pristine">
        <h5>Closes 5 bus stops ({{ busstops.length }})</h5>
        <ul>
            <li v-for="busstop in busstops">
                {{ busstop.name }}
            </li>
        </ul>

        <h5>Schools within 20km radius ({{ schools.length }})</h5>
        <ul>
            <li v-for="school in schools">
                {{ school.name }}
            </li>
        </ul>

        <h5>All addresses on this postcode ({{ addresses.length }})</h5>
        <ul>
            <li v-for="address in addresses">
                {{ address.full }}
            </li>
        </ul>
    </div>
</div>