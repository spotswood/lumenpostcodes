<div class="panel panel-default">
    <div class="panel-heading" role="tab" id="heading-<?php echo($group->getId()) ?>">
        <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion"
               href="#collapse-<?php echo($group->getId()) ?>" aria-expanded="true"
               aria-controls="collapse-<?php echo($group->getId()) ?>">
                <?php echo($group->getName()) ?>
            </a>
        </h4>
    </div>
    <div id="collapse-<?php echo($group->getId()) ?>" class="panel-collapse collapse in"
         role="tabpanel" aria-labelledby="heading-<?php echo($group->getId()) ?>">
        <div class="panel-body">
            <?php foreach ($group->getSubgroups() as $subgroup) { require('hierarchy-group.php'); } ?>
        </div>
    </div>
</div>