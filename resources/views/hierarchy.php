<?php require('layout/header.php'); ?>

    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                    <?php foreach ($hierarchy as $group) : ?>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="heading-<?php echo($group->getId()) ?>">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion"
                                       href="#collapse-<?php echo($group->getId()) ?>" aria-expanded="true"
                                       aria-controls="collapse-<?php echo($group->getId()) ?>">
                                        <?php echo($group->getName()) ?> (<?php echo($group->getTotalPostcodesCount()); ?>)
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse-<?php echo($group->getId()) ?>" class="panel-collapse collapse in"
                                 role="tabpanel" aria-labelledby="heading-<?php echo($group->getId()) ?>">
                                <div class="panel-body">
                                    <?php foreach ($group->getSubgroups() as $subgroup) : ?>
                                        <h3><?php echo($subgroup->getName()) ?> (<?php echo(count($subgroup->getPostcodes())) ?>)</h3>
                                        <div class="subgroup-list">
                                        <?php foreach ($subgroup->getPostcodes() as $postcode) : ?>
                                            <button class="btn btn-default btn-xs btn-postcode" id="<?php echo($postcode->getId()); ?>"><?php echo($postcode->getPostcode()); ?></button>
                                        <?php endforeach; ?>
                                        </div>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
            <div class="col-md-3">
                <?php require('partials/sidebar.php'); ?>
            </div>
        </div>
    </div>

<?php require('layout/footer.php'); ?>