<?php require('layout/header.php'); ?>

    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <a href="<?php echo(route('groups.hierarchy')); ?>" class="btn btn-primary btn-lg btn-block">View postcode hierarchy</a>
            </div>
        </div>
    </div>

<?php require('layout/footer.php'); ?>